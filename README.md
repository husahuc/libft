# Super-libft
Projet fait pour l'ecole 42 Lyon, consistant a faire une bibliothèque de fonctions basiques en c pour les réutiliser plus tard.

# Objectif
Revoir les basiques du language c.
Faire des fonctions solides pour toutes utilisations.

# Fonctions disponibles
- Str
    - New pour allouer une nouvelle chaine (ft_strnew ...)
    - Modify pour changer une chaine (mettre en majuscule ...)
    - Put pour afficher la chaine, ou un charactere
    - Split-join pour split, join ou autre des chaines pour en créer une nouvelle (ft_strsplit.c ...)
    - Char pour toutes les opérations sur un charactére
    - Functions pour toutes les opérations sur une chaine avec une fonction.
    - Test sur une chaine (longueur avec strlen ...).
    - Free une chaine
- Mem pour les fonctions sur la mémoire (créer, dupliquer, supprimer...)
- Int pour les fonctions sur les nombres (obtenir un nombre à partir d'une chaine de charactére)
- GNL pour get next line, permettant d'avoir une ligne (finit par un '\n') sur un file descriptor

# a faire
- [x] remake makefile with folder.
- [x] Super Function like ft_option.
- Functions with points.
- Quick sort
- Unix utilities
- [x] base functions for printf

