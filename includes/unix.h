/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   unix.h                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/12 13:17:27 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/11/28 15:21:20 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef UNIX_H
# define UNIX_H
# include <stdlib.h>
# include <unistd.h>

ssize_t		ft_options(char **str, char *letters);
#endif
