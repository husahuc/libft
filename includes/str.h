/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   str.h                                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/28 15:07:08 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/11/28 15:15:27 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef STR_H
# define STR_H
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>

/*
** Variable
*/

# define INFINITE 1

/*
** put functions
*/

void		ft_putchar(char c);
void		ft_putchar_fd(char c, int fd);

void		ft_putendl(char const *s);
void		ft_putendl_fd(char const *s, int fd);

void		ft_putstr(char const *s);
void		ft_putstr_fd(char const *s, int fd);

void		ft_putnbr(int n);
void		ft_putnbr_fd(int n, int fd);

/*
** test
*/

size_t		ft_strlen(const char *str);

int			ft_count_block(char const *str, char c);
int			ft_count_word(char const *str, char c, unsigned int i);

int			ft_strcmp(const char *s1, const char *s2);
int			ft_strncmp(const char *s1, const char *s2, size_t n);

int			ft_strequ(char const *s1, char const *s2);
int			ft_strnequ(char const *s1, char const *s2, size_t n);

char		ft_strcchr(const char *str, int c);
char		*ft_strrchr(const char *str, int c);

char		*ft_strstr(char *str, char *to_find);
char		*ft_strchr(const char *str, int c);

/*
** split_join
*/

char		*ft_strjoin(char const *s1, char const *s2);
char		*ft_strjoinf(char *s1, char *s2, int mode);
void		ft_strjoin_free(char **s1, const char *s2);

char		**ft_strsplit(char const *s, char c);

char		*ft_strsub(char const *s, unsigned int start, size_t len);

char		*ft_strtrim(char const *s);

char		*ft_strncpy(char *dest, const char *src, size_t n);

char		*ft_strcat(char *dest, const char *src);
char		*ft_strncat(char *dest, char *src, int nb);
size_t		ft_strlcat(char *dest, const char *src, size_t n);

char		*ft_strnstr(const char *str, const char *to_find, size_t len);

/*
** new
*/

char		*ft_strcpy(char *dest, char *src);
char		*ft_strdup(const char *src);
char		*ft_strnew(size_t size);

/*
** Modify
*/

char		*ft_strcapitalize(char *str);
char		*ft_strlowcase(char *str);
char		*ft_strupcase(char *str);

/*
** function
*/

void		ft_striter(char *s, void (*f)(char *));
void		ft_striteri(char *s, void (*f)(unsigned int, char *));

char		*ft_strmap(char const *s, char (*f)(char));
char		*ft_strmapi(char const *s, char (*f)(unsigned int, char));

char		**ft_sort_list_world(int (*ft)(char *, char*),
			char **tab_char, int len);

/*
** free
*/

void		ft_bzero(void *s, size_t n);
void		ft_strclr(char *s);
void		ft_strdel(char **as);

/*
** char
*/

int			ft_isalpha(int c);
int			ft_isascii(int c);
int			ft_isdigit(int c);
int			ft_isprint(int c);

int			ft_isalnum(int c);
int			ft_isalnum_underscore(char *str);

int			ft_tolower(int c);
int			ft_toupper(int c);

#endif
