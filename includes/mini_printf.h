/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   mini_printf.h                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/28 15:12:05 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/11/28 15:18:35 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef MINI_PRINTF_H
# define MINI_PRINTF_H

# include <stdarg.h>

int			ft_printf(const char *argc, ...);

int			ft_printf_err(const char *str, ...);

/*
** types
*/

void		mini_printf_string(va_list list, int fd);
void		mini_printf_char(va_list list, int fd);
void		mini_printf_int(va_list list, int fd);
void		mini_printf_tab(va_list list, int fd);

int			mini_printf_options(const char *str, va_list list, int fd);
#endif
