/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   sort_list_world.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/12 16:04:37 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/13 17:11:08 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

char		**ft_sort_list_world(int (*ft)(char *, char*),
	char **tab_char, int len)
{
	int		i;
	int		j;
	char	*buf;

	i = 0;
	while (i < len)
	{
		j = i + 1;
		while (j < len)
		{
			if (ft(tab_char[i], tab_char[j]) == 1)
			{
				buf = tab_char[i];
				tab_char[i] = tab_char[j];
				tab_char[j] = buf;
			}
			j++;
		}
		i++;
	}
	return (tab_char);
}
