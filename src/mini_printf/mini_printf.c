/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   mini_printf.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/28 15:11:58 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/10/28 15:12:00 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "mini_printf.h"
#include "str.h"

int			mini_printf_options(const char *str, va_list list, int fd)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '%')
		{
			i++;
			if (str[i] == 's')
			{
				mini_printf_string(list, fd);
				i++;
			}
			else if (str[i] == 'c')
			{
				mini_printf_char(list, fd);
				i++;
			}
			else if (str[i] == 'd')
			{
				mini_printf_int(list, fd);
				i++;
			}
			else if (str[i] == 't')
			{
				mini_printf_tab(list, fd);
				i++;
			}
			else if (str[i] == '%')
			{
				ft_putchar_fd('%', fd);
				i++;
			}
		}
		else
		{
			ft_putchar_fd(str[i], fd);
			i++;
		}
	}
	return (0);
}

int			ft_printf(const char *str, ...)
{
	va_list	params;

	va_start(params, str);
	mini_printf_options(str, params, 1);
	va_end(params);
	return (0);
}
