/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   mini_printf.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/28 15:11:58 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/10/28 15:12:00 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "mini_printf.h"
#include "str.h"

void		mini_printf_string(va_list list, int fd)
{
	char	*str;

	str = va_arg(list, char *);
	ft_putstr_fd(str, fd);
}

void		mini_printf_char(va_list list, int fd)
{
	char	c;

	c = (unsigned char) va_arg(list, int);
	ft_putchar_fd(c, fd);
}

void		mini_printf_int(va_list list, int fd)
{
	int		x;

	x = va_arg(list, int);
	ft_putnbr_fd(x, fd);
}

void		mini_printf_tab(va_list list, int fd)
{
	char **tab;
	int i;

	tab = (char **)va_arg(list, char **);
	i = 0;
	while (tab[i] != NULL)
	{
		ft_putstr_fd(tab[i], fd);
		ft_putchar_fd(' ', fd);
		i++;
	}
}
