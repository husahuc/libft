/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_free_tab.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/29 10:52:54 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/10/29 10:52:55 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "tab.h"
#include "str.h"

void	ft_free_tab(char **tabl)
{
	int i;

	i = 0;
	if (tabl != NULL)
	{
		while (tabl[i])
		{
			ft_strdel(&tabl[i]);
			tabl[i] = NULL;
			i++;
		}
		ft_strdel(&tabl[i]);
		free(tabl);
		tabl = NULL;
	}
}
